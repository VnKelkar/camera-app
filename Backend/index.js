import express from 'express';
import cors from "cors";
const app = express();
import videoRoutes from './routes/index.js'

const startServer = async () => {
    app.use(cors());
    app.use(express.json({limit: '50mb'}));
    try {
      app.listen(4000);
      //registerRoutes(app);
      // app.use(globalErrorHandler);
      console.info("SERVER STARTED");
      app.use('/save', videoRoutes);
    } catch (e) {
      console.info("COULD NOT START SERVER");
      process.exit(1);
    }
  };
  
  startServer();
  